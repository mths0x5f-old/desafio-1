package challenge;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

  private final String nome;
  private final String cpf;
  private final int idade;
  private final String sexo;

  public Person(@JsonProperty("nome") String nome, @JsonProperty("cpf") String cpf,
      @JsonProperty("idade") int idade, @JsonProperty("sexo") String sexo) {
    this.nome = nome;
    this.cpf = cpf;
    this.idade = idade;
    this.sexo = sexo;
  }

  public String getNome() {
    return nome;
  }

  public String getCpf() {
    return cpf;
  }

  public int getIdade() {
    return idade;
  }

  public String getSexo() {
    return sexo;
  }

  @Override
  public String toString() {
    return String.format("%s: %s, %s, %s", this.cpf, this.nome, this.idade, this.sexo);
  }

}
