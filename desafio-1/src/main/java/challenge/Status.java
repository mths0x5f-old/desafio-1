package challenge;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Status {

  SUCCESS(0), FAILURE(1);

  private final int status;

  private Status(int status) {
    this.status = status;
  }

  public int getStatus() {
    return status;
  }

}
