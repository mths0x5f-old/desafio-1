package challenge;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

  private PersonDAO peopleManager = new PersonDAO();
  private Logger log = Logger.getLogger(PersonController.class.getName());

  @RequestMapping(value = "/pessoas/add", method = RequestMethod.POST)
  public Status addPerson(@RequestBody Person person) {

    try {
      peopleManager.add(person);
    } catch (DAOException e) {
      log.warn(e);
      return Status.FAILURE;
    }
    return Status.SUCCESS;

  }

  @RequestMapping(value = "/pessoas/qnome", method = RequestMethod.GET)
  public ArrayList<Person> findByName(@RequestParam(value = "nome") String name) {
    return peopleManager.getByName(name);
  }

  @RequestMapping(value = "/pessoas/qcpf", method = RequestMethod.GET)
  public Person findByCPF(@RequestParam(value = "cpf") String cpf) {
    return peopleManager.getByCPF(cpf);
  }
  
  @RequestMapping(value = "/pessoas/todas")
  public ArrayList<Person> listAll() {
    return peopleManager.getAllPersons();
  }

}
