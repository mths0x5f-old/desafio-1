package challenge;

public class DAOException extends RuntimeException {

  private static final long serialVersionUID = 6890340959250172318L;

  public DAOException() {
    super();
  }
  
  public DAOException(String message) {
    super(message);
  }

}
