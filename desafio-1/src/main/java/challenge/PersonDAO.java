package challenge;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class PersonDAO {

  private Map<String, Person> people = new Hashtable<>();

  public void add(Person person) {

    // Um caso de erro para dar status=1
    if(people.containsKey(person.getCpf())) {
      throw new DAOException("CPF already registered.");
    }
    people.put(person.getCpf(), person);

  }

  public ArrayList<Person> getAllPersons() {
    return new ArrayList<Person>(people.values());
  }
  
  public Person getByCPF(String cpf) {
    return people.get(cpf);
  }

  public ArrayList<Person> getByName(String name) {

    ArrayList<Person> found = new ArrayList<>();
    
    for (Person p : people.values()) {
      if (p.getNome().toLowerCase().contains(name.toLowerCase()))
        found.add(p);
    }
    return found;
    
  }

}
