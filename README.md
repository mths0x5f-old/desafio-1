# Desafio-1 #

1. Serviço que recebe um objeto via JSON e guarda o mesmo em memória.

2. Serviço que recebe uma requisição GET com o parâmetro "nome" e retorna o objeto correspondente em JSON.

3. Serviço que recebe uma requisição GET com o parâmetro "cpf" e retorna o objeto correspondente em JSON.
## URLs ##
* /pessoas/add
* /pessoas/qnome?nome=
* /pessoas/qcpf?cpf=
* /pessoas/todas